<?php

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
	}

	/**
	 * Generate offices list
	 *
	 * @return json
	 */

	public function offices(){
		$offices = $this->api_model->get_offices_list();
		$employees = $this->api_model->get_employee_list();

		if(!empty($offices)){
			foreach ($offices as $officeKey => $office) {
				if(!empty($employees)){
					array_walk($employees,function($value, $key) use (&$employees, &$office, &$officeKey, &$offices){
						if($office['officeCode'] == $value['officeCode']){
							//unset not used field
					        unset($offices[$officeKey]['reportsTo']);
					        unset($value['reportsTo']);
					        unset($value['officeCode']);
							$offices[$officeKey]['employees'][] = $value;
						}
					});
				}
			}
		}
		header('Content-Type: application/json');
		echo json_encode($offices, JSON_PRETTY_PRINT);
	}

	/**
	 * Generate organization tree
	 *
	 * @return json
	 */

	public function organization(){

		$employees = $this->api_model->get_employee_list();

		$employeeGroup = array();
		foreach ($employees as $a){
		    $employeeGroup[$a['reportsTo']][] = $a;
		}

		$result = $this->generate_array_tree($employeeGroup, array($employees[0]));

		header('Content-Type: application/json');
		echo json_encode($result[0], JSON_PRETTY_PRINT);
	}

	/**
	 * Generate array tree
	 * @param array employeeGroup
	 * @param array parent
	 * @return array
	 */

	public function generate_array_tree(&$employeeGroup, $parent){
	    $array_tree = array();
	    foreach ($parent as $k=>$l){
	        if(isset($employeeGroup[$l['employeeNumber']])){
	            $l['employeeUnder'] = $this->generate_array_tree($employeeGroup, $employeeGroup[$l['employeeNumber']]);
	        }
	        //unset not used field
	        unset($l['reportsTo']);
	        unset($l['officeCode']);
	        $array_tree[] = $l;
	    } 
	    return $array_tree;
	}

	/**
	 * Generate employee sales report
	 * @param int employeeNumber
	 * @return json
	 */

	public function sales_report($employeeNumber = null){

		if(!empty($employeeNumber)){
			$employeeReports = $this->generate_employee_sales_report($employeeNumber);
			if(!isset($employeeReports['employeeNumber'])){
				$employeeReports = array("error" => true, "message" => "Employee not found.");
				header('Content-Type: application/json');
				echo json_encode($employeeReports, JSON_PRETTY_PRINT);exit();
			}

			$productLineReports = $this->generate_product_line_sales_report($employeeNumber);
			$employeeReports['productLines'] = [];
			if(isset($productLineReports[0]['productLine'])){
				$employeeReports['productLines'] = $productLineReports;
			}
			if(!empty($employeeReports['productLines'])){
				foreach ($employeeReports['productLines'] as $key => $prodLine) {
					$productReports = [];
					if(isset($prodLine['productLine'])){
						$productReports = $this->generate_product_sales_report($employeeNumber, $prodLine['productLine']);
					}
					$employeeReports['productLines'][$key]['products'] = $productReports;
				}
			}
		} else {
			$employeeReports = $this->generate_employee_sales_report();
			if(!empty($employeeReports)){
				foreach ($employeeReports as $key => $employeeReport) {
					$productLineReports = $this->generate_product_line_sales_report($employeeReport['employeeNumber']);
					$employeeReports[$key]['productLines'] = [];
					if(isset($productLineReports[0]['productLine'])){
						$employeeReports[$key]['productLines'] = $productLineReports;
						if(!empty($employeeReports[$key]['productLines'])){
							foreach ($employeeReports[$key]['productLines'] as $pkey => $prodLine) {
								
								$productReports = [];
								if(isset($prodLine['productLine'])){
									$productReports = $this->generate_product_sales_report($employeeReport['employeeNumber'], $prodLine['productLine']);
								}
								$employeeReports[$key]['productLines'][$pkey]['products'] = $productReports;
							}
						}
					}
				}
			}
		}

		header('Content-Type: application/json');
		echo json_encode($employeeReports, JSON_PRETTY_PRINT);
	}

	/**
	 * Generate employee sales report
	 * @param int employeeNumber
	 * @return array
	 */

	public function generate_employee_sales_report($employeeNumber = null){
		return  $this->api_model->generate_employee_sales_report($employeeNumber);
	}

	/**
	 * Generate product line sales report
	 * @param int employeeNumber
	 * @return array
	 */

	public function generate_product_line_sales_report($employeeNumber){
		return  $this->api_model->generate_product_line_sales_report($employeeNumber);
	}

	/**
	 * Generate product sales report
	 * @param int employeeNumber
	 * @param int productLine
	 * @return array
	 */

	public function generate_product_sales_report($employeeNumber, $productLine){
		return  $this->api_model->generate_product_sales_report($employeeNumber, $productLine);
	}
}