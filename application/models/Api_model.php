<?php
/**
 * 
 */
class Api_model extends CI_Model
{
	
	public function __construct()
	{
		$this->load->database();
	}

	public function get_employee_list(){
		$this->db->select('employeeNumber, jobTitle, reportsTo, officeCode, CONCAT(firstName, '.' " " '.', lastName) AS name', FALSE);
		$query = $this->db->get('employees')->result_array();
		return $query;
	}

	public function get_offices_list(){
		$this->db->select('officeCode,city');
		$query = $this->db->get('offices')->result_array();
		return $query;
	}

	public function get_employee_info($employeeNumber = null){
		$where = "";
		if(!empty($employeeNumber)){
			$where = "where emp.employeeNumber = ". "{$employeeNumber}";
		}

		$sql=<<<EOD
SELECT 
	emp.employeeNumber, CONCAT(emp.lastName,' ', emp.firstName) as name, emp.jobTitle, emp.officeCode, off.city
    FROM `employees` as emp
    left JOIN offices as off on off.officeCode = emp.officeCode
    {$where}
EOD;
		$rows=$this->run_raw_select_sql_array($sql);
		return $rows;

	}

	public function get_multiple_row_array($qry)
	{
		if ($qry && $qry->num_rows() > 0) {
			$rows=$qry->result_array();
			$qry->free_result();
			return $rows;
		}
		return null;
	}

	public function run_raw_select_sql_array($sql, $params = null, $db=null)
	{
		if(empty($db)){
			$db=$this->db;
		}
		$qry = $db->query($sql, $params);
		$rlt= $this->get_multiple_row_array($qry);
		unset($qry);
		return $rlt;
	}

	public function generate_employee_sales_report($employeeNumber = null){

		$params = "group by emp.`employeeNumber`";
		if(!empty($employeeNumber)){
			$params = "WHERE employeeNumber = {$employeeNumber}";
		}

		$sql=<<<EOD
SELECT 
#*
#prod.`productCode`,

emp.`employeeNumber`,
CONCAT(emp.`firstName`, " ", emp.`lastName`) AS NAME,
emp.`jobTitle`,
emp.`officeCode`,
offc.`city`,

#odd.`quantityOrdered`,
#odd.`priceEach`,
#odd.`productCode`,
#prod.`productScale`,
#round((SUBSTRING_INDEX(prod.productScale, ":",1))/SUBSTRING_INDEX(prod.productScale, ":",-1),16) as ratio,
SUM(ROUND(odd.`quantityOrdered` * odd.`priceEach`,2)) AS totalSales,
SUM(ROUND((odd.`quantityOrdered` * odd.`priceEach`) * ROUND((SUBSTRING_INDEX(prod.productScale, ":",1))/SUBSTRING_INDEX(prod.productScale, ":",-1),16),2)) AS totalCommision
FROM employees AS emp
LEFT JOIN offices AS offc ON offc.`officeCode` = emp.`officeCode`
LEFT JOIN customers AS cust ON cust.`salesRepEmployeeNumber` = emp.`employeeNumber`
LEFT JOIN orders AS ors ON ors.`customerNumber` = cust.`customerNumber`
LEFT JOIN orderdetails AS odd ON odd.`orderNumber` = ors.`orderNumber`
LEFT JOIN products AS prod ON prod.`productCode` = odd.`productCode`
LEFT JOIN productlines AS prodl ON prodl.`productLine` = prod.`productLine`

{$params}
EOD;

		$rows=$this->run_raw_select_sql_array($sql);
		if(!empty($employeeNumber)){
			return $rows[0];
		}
		return $rows;
	}

	public function generate_product_line_sales_report($employeeNumber){
		$sql=<<<EOD
SELECT 
prodl.`productLine`,
prodl.`textDescription`,
SUM(ROUND((odd.`quantityOrdered` * odd.`priceEach`) * ROUND((SUBSTRING_INDEX(prod.productScale, ":",1))/SUBSTRING_INDEX(prod.productScale, ":",-1),16),2)) AS commission,
SUM(odd.`quantityOrdered`) AS quantity,
SUM(ROUND(odd.`quantityOrdered` * odd.`priceEach`,2)) AS sales

FROM employees AS emp
LEFT JOIN offices AS offc ON offc.`officeCode` = emp.`officeCode`
LEFT JOIN customers AS cust ON cust.`salesRepEmployeeNumber` = emp.`employeeNumber`
LEFT JOIN orders AS ors ON ors.`customerNumber` = cust.`customerNumber`
LEFT JOIN orderdetails AS odd ON odd.`orderNumber` = ors.`orderNumber`
LEFT JOIN products AS prod ON prod.`productCode` = odd.`productCode`
LEFT JOIN productlines AS prodl ON prodl.`productLine` = prod.`productLine`

WHERE employeeNumber = {$employeeNumber} 
GROUP BY prodl.`productLine`
EOD;

		$rows=$this->run_raw_select_sql_array($sql);
		return $rows;
	}

	public function generate_product_sales_report($employeeNumber, $productLine){
		$sql=<<<EOD
SELECT 
prod.`productCode`,
prod.`productName`,
SUM(odd.`quantityOrdered`) AS quantity,
SUM(ROUND(odd.`quantityOrdered` * odd.`priceEach`,2)) AS sales,
COUNT(cust.`customerNumber`) AS numberOfCustomerBought

FROM employees AS emp
LEFT JOIN offices AS offc ON offc.`officeCode` = emp.`officeCode`
LEFT JOIN customers AS cust ON cust.`salesRepEmployeeNumber` = emp.`employeeNumber`
LEFT JOIN orders AS ors ON ors.`customerNumber` = cust.`customerNumber`
LEFT JOIN orderdetails AS odd ON odd.`orderNumber` = ors.`orderNumber`
LEFT JOIN products AS prod ON prod.`productCode` = odd.`productCode`
LEFT JOIN productlines AS prodl ON prodl.`productLine` = prod.`productLine`

WHERE employeeNumber = {$employeeNumber} AND prodl.`productLine` = "{$productLine}"
GROUP BY prod.`productCode`;
EOD;


		$rows=$this->run_raw_select_sql_array($sql);
		return $rows;
	}
}